// Callum Perks
// Mesh.cpp
// Contains the implementation of the Mesh class

#include <Interaction/MousePicker.h>
#include "pch.h"
#include "Mesh.h"


Mesh::Mesh(std::vector<Vertex *> &vertices, std::map<std::pair<int, int>, HalfEdge *> &edges, std::vector<Face *> &faces, std::vector<QVector3D> normals)
    : vertices(vertices), edgesMap(edges), faces(faces), normals(normals) {
    for(auto edge : edges)
        this->edges.push_back(edge.second);
}

/**
 * Creates a Mesh object from given OBJ file (Half edge mesh)
 *
 * @param filename The name of the OBJ file for which the mesh is being created
 * @return A mesh object containing the half edge representation of the OBJ file
 */
Mesh Mesh::creatFromObj(const std::string &filename) {
    std::ifstream file(filename);
    std::string line;

    std::vector<Vertex*> vertices;
    std::vector<Face*> faces;
    std::vector<QVector3D> normals;

    //loop through each line of the file parsing them
    if (file.is_open()) {
        while (getline(file, line)) {
            parseLine(line, vertices, faces, normals);
        }

        file.close();
    } else{
        std::cerr << "Could not open file: " << filename << std::endl;
    }


    std::map<std::pair<int, int>, HalfEdge*> edges = createHalfEdges(vertices, faces);

    return Mesh(vertices, edges, faces, normals);
}

Mesh::~Mesh() {
    vertices.clear();
    edgesMap.clear();
    faces.clear();
}

Mesh::Mesh() {
}

/**
 * Parses a given string of text, constructing any vertices or faces and string them in the given vectors
 *
 * @param line The line of text that is being parsed
 * @param vertices The vector of vertices in which the new vertices will be stored
 * @param faces The vector of faces in which the new faces will be stored
 */
void parseLine(const std::string &line, std::vector<Vertex *> &vertices, std::vector<Face *> &faces,
               std::vector<QVector3D>& normals) {
    //construct vertex if the line starts with 'v'
    if (line[0] == 'v') {
        if(line[1] == ' ') {
            Vertex *vertex = new Vertex();
            //read in the position of the vertex
            float x, y, z;
            sscanf(line.c_str(), "v %f %f %f", &x, &y, &z);
            vertex->originalPosition.setX(x);
            vertex->originalPosition.setY(y);
            vertex->originalPosition.setZ(z);
            vertex->originalPosition.setW(1.0f);

            vertex->currentPosition = QVector4D(vertex->originalPosition);

            vertices.push_back(vertex);
        } else {
            //construct normal, read in the position
            float x, y, z;
            sscanf(line.c_str(), "vn %f %f %f", &x, &y, &z);
            normals.emplace_back(QVector3D(x, y, z));
        }
        //construct face if the line starts with 'f'
    } else if (line[0] == 'f') {
        Face* face = new Face();
        std::vector<std::string> lineSplit;

        //get the vertex ids for drawing the mesh
        for (unsigned int i = 2; i < line.size();) {
            int j = 1;
            if (line[i] != ' ') {
                std::string index;
                index += line[i];
                //loop through the rest of the number
                while((i+j-1) != (line.size() - 1) && line[i + j] != ' ') {
                    index += line[i + j];
                    j++;
                }
                lineSplit.push_back(index);
            }
            i += j;
        }

        //loop through each vertex
        for(const std::string& string : lineSplit){
            std::string vertexIndex;
            std::string normalIndex;
            int normStartIndex = 0;
            for(unsigned int i = 0; i < string.length(); i++){
                //add the character to the vertex index if it is not a forward slash
                if(string[i] != '/' ){
                    vertexIndex += string[i];
                } else {
                    //skip the forward slashes
                    normStartIndex = i + 2;
                    break;
                }
            }
            face->vertexID.push_back(std::stoi(vertexIndex) - 1);

            //loop through the rest of the string to get the normal index
            for(unsigned int i = normStartIndex; i < string.length(); i++){
                normalIndex += string[i];
            }

            face->normalID = (std::stoi(normalIndex) - 1);
        }
        faces.push_back(face);
    }

}

/**
 * Takes a given vector of vertices and faces and constructs the half edges for those vectors
 *
 * @param vertices The vector containing the vertices of the half edges that are being constructed
 * @param faces The vector containing the faces of the half edges that are being constructed
 * @return A map of pairs of vertex ids to half edges
 */
std::map<std::pair<int, int>, HalfEdge*> createHalfEdges(std::vector<Vertex*> &vertices, std::vector<Face*> &faces) {
    std::map<std::pair<int, int>, HalfEdge*> edges;

    for (Face* face : faces) {
        std::vector<int> vertexID = face->vertexID;
        //loop through each vertex pair creating edgesMap
        for (unsigned int i = 0; i < vertexID.size(); i++) {
            //calculate the index where the next vertex is stored
            int j = (i + 1) % vertexID.size();

            HalfEdge* edge = new HalfEdge();
            edge->vertex = vertices[vertexID[i]];
            edge->face = face;
            edges[{vertexID[i], vertexID[j]}] = edge;

            //only assign the edge to the face if it is the first edge being constructed
            if(i == 0)
                face->edge = edge;

            vertices[vertexID[i]]->outEdge = edge;
        }


        //loop through each edge setting next/previous/twin edge
        for (unsigned int vertexNum = 0; vertexNum < vertexID.size(); vertexNum++) {
            int j = vertexNum == (vertexID.size() - 1) ? vertexID[0] : vertexID[vertexNum + 1];
            int k = vertexNum == (vertexID.size() - 2) ? vertexID[0] : vertexNum == (vertexID.size() - 1) ? vertexID[1] : vertexID[vertexNum + 2];
            int i = vertexID[vertexNum];

            edges[{i,j}]->nextEdge = edges[{j,  k}];

            //set twin edgesMap when both edgesMap have been created
            if(edges.find({j,i}) != edges.end()) {
                edges[{i, j}]->twinEdge = edges[{j, i}];
                edges[{j, i}]->twinEdge = edges[{i, j}];
            }
        }
    }

    return edges;
}

void Mesh::resetVertices() {
    for(Vertex * vertex: vertices)
        vertex->currentPosition = vertex->originalPosition;

}

/**
 * Calculates the degree/valence of a given vertex
 *
 * @param vertex The vertex for which the degree/valence should be calculated
 *
 * @return The degree/valence
 */
int Mesh::calculateVertexDegree(Vertex *vertex) {
    int degree = 0;

    //loop through each edge adjacent to the vertex and increment the degree/valence
    HalfEdge* edge = vertex->outEdge;
    do{
        degree++;
        edge = edge->twinEdge->nextEdge;
    } while(edge != vertex->outEdge);

    return degree;
}

/**
 * Creates a face from a list of vertices
 *
 * @param faceVertices The list of vertices contained in the face
 */

void Mesh::createFace(const std::vector<Vertex *>& faceVertices, int normalID) {
    int faceVerticesSize = faceVertices.size();
    Face *face = new Face();

    //same number of edges as vertices
    std::vector<HalfEdge *> edges;
    edges.reserve(faceVertices.size());
    this->edges.reserve(faceVertices.size());

    for (int i = 0; i < faceVerticesSize; i++)
        edges.emplace_back(new HalfEdge());

    for (int i = 0; i < faceVerticesSize; i++) {
        edges[i]->vertex = faceVertices[i];
        edges[i]->face = face;
        edges[i]->nextEdge = edges[(i + 1) % faceVertices.size()];
        faceVertices[i]->outEdge = edges[i];
        this->edges.push_back(edges[i]);


    }

    face->edge = edges[0]; //The edge doesn't matter so just use the first oe
    this->faces.push_back(face);

    //connect edge pairs
    for (int i = 0; i < faceVerticesSize; i++) {
        for (HalfEdge *edge : this->edges) {
            Vertex* vertex = faceVertices[i];
            //they are twin edges if next vertices for each edge is the same as the vertex for each edge
            if ((vertex->outEdge->nextEdge->vertex == edge->vertex) &&
                (vertex == edge->nextEdge->vertex)) {
                edge->twinEdge = vertex->outEdge;
                vertex->outEdge->twinEdge = edge;
            }
        }
    }

    //calculate normal
    QVector3D edge1 = QVector3D(faceVertices[0]->currentPosition- faceVertices[1]->currentPosition);
    QVector3D edge2 = QVector3D(faceVertices[1]->currentPosition - faceVertices[2]->currentPosition);
    QVector3D normal = QVector3D::normal(edge1, edge2);

    face->normalID = normalID;
    normals.push_back(normal);

}

void Mesh::recalculateNormals() {
    Vertex* vertex = MousePicker::currentPickedVertex();
    HalfEdge* edge = vertex->outEdge;
    do{
        HalfEdge* nextEdge = edge->nextEdge;
        HalfEdge* nextNextEdge = nextEdge->nextEdge;
        Face* face = edge->face;

        QVector3D edgeVector = QVector3D(nextEdge->vertex->currentPosition - edge->vertex->currentPosition);
        QVector3D nextEdgeVector = QVector3D(nextNextEdge->vertex->currentPosition - nextEdge->vertex->currentPosition);

        normals[face->normalID] = QVector3D::normal(edgeVector, nextEdgeVector);
        edge = edge->twinEdge->nextEdge;
    } while(edge != vertex->outEdge);

}



