// Callum Perks
// GLcontext.cpp
// Contains the implementation of the GLContext class

#include "pch.h"
#include "Interaction/ArcBall.h"
#include "Interaction/MousePicker.h"
#include "GLContext.h"
#include "Settings.h"

//TODO: resizing Window Support

GLContext::GLContext() : widgetWidth(width()), widgetHeight(height()), zoom(-2) {
    setMouseTracking(true);
    mesh = Mesh::creatFromObj(std::string("../Catmull_Clark/res/cube.obj"));
    subdivision = MeshSubdivision(&mesh);

    setFocusPolicy(Qt::StrongFocus);
}

GLContext::~GLContext() {

}

/**
 * Called when the openGL context is set up
 */
void GLContext::initializeGL() {
    //set the clear color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    glViewport(0, 0, this->width(), this->height());

    //enable depth testing
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0f, (float) this->width() / this->height(), 0.0001f, 100.0f);

    //information for the light
    float lightPos[] = {-1.0f, 1.0f, 1.0f, 0.0f};
    float ambient[] = {0.1f, 0.1f, 0.1f, 1.0f};
    float diffuse[] = {0.8f, 0.8f, 0.8f, 1.0f};
    float specular[] = {0.9f, 0.9f, 0.9f, 1.0f};

    //enable lighting
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glShadeModel(GL_SMOOTH);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
    glLightf(GL_FRONT, GL_SPOT_EXPONENT, 100.0f);
}

/**
 * Called whenever the OS needs the screen to be redrawn
 */
void GLContext::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    drawMesh(subdivision.getMesh());

    //draw wireframe of original mesh if the original mesh is not the current one
    if(subdivision.getSubdivisionLevel() != 0)
        drawOriginalMesh(subdivision.getOriginalMesh());

}

/**
 * Draws a given mesh to the screen
 *
 * @param mesh The mesh to be drawn
 */
void GLContext::drawMesh(const Mesh &mesh) {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0, 0, zoom);
    glMultMatrixf(ArcBall::rotation.data());
    glColor3f(1, 1, 1);

    //set if the mesh is drawn as wireframe or not
    glPolygonMode(GL_FRONT_AND_BACK, Settings::wireframe ? GL_LINE : GL_FILL);


    if (Settings::wireframe)
        glDisable(GL_LIGHTING);

    //loop through each face to draw them
    for (Face *face : mesh.faces)
        drawFace(face);

    //disable lighing to mke the points easier to see
    glDisable(GL_LIGHTING);

    //Render vertices as spheres for easy clicking
    GLUquadric *sphere = gluNewQuadric();
    for (Vertex *vertex : mesh.vertices) {
        if (vertex == MousePicker::currentPickedVertex())
            glColor3f(1, 0, 1);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glTranslatef(vertex->currentPosition.x(), vertex->currentPosition.y(), vertex->currentPosition.z());
        gluSphere(sphere, abs((int) vertex->currentPosition.z()) * 0.01 - (zoom * 0.001), 50, 50);
        glPopMatrix();
        glColor3f(1, 1, 1);
    }

    glColor3f(0, 1, 0);
    //render the face points
    if (Settings::facePoints) {
        std::vector<Face *> &faces = subdivision.getMesh().faces;

        glPointSize(20 + (zoom));
        glBegin(GL_POINTS);
        for (Face *face : faces) {
            glVertex3f(face->facePoint->currentPosition.x(), face->facePoint->currentPosition.y(),
                       face->facePoint->currentPosition.z());
        }
        glEnd();
    }

    //render the edge points
    glColor3f(0, 0, 1);
    if (Settings::edgePoints) {
        std::vector<HalfEdge *> &edges = subdivision.getMesh().edges;

        glPointSize(20 + (zoom));
        glBegin(GL_POINTS);
        for (HalfEdge *edge: edges) {
            glVertex3f(edge->edgePoint->currentPosition.x(), edge->edgePoint->currentPosition.y(),
                       edge->edgePoint->currentPosition.z());
        }
        glEnd();
    }

    //render the vertex points
    glColor3f(1, 1, 0);
    if (Settings::vertexPoints) {
        std::vector<Vertex *> &vertices = subdivision.getMesh().vertices;
        glPointSize(20 + (zoom));
        glBegin(GL_POINTS);
        for (Vertex *vertex: vertices) {
            glVertex3f(vertex->newVertex->currentPosition.x(), vertex->newVertex->currentPosition.y(),
                       vertex->newVertex->currentPosition.z());
        }
        glEnd();
    }
    glEnable(GL_LIGHTING);
}

/**
 * Draws a wireframe of the original mesh
 *
 * @param mesh The mesh to be drawn
 */
void GLContext::drawOriginalMesh(const Mesh &mesh) {
    glColor3f(1, 1, 1);

    glDisable(GL_LIGHTING);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    for (Face *face : mesh.faces) {
        glBegin(GL_POLYGON);
        HalfEdge *edge = face->edge;
        int normalIndex = 0;

        QVector3D normal = mesh.normals[face->normalID];
        do {
            //get the vertex for the current edge
            glNormal3f(normal.x(), normal.y(), normal.z());
            Vertex *vertex = edge->vertex;

            //give the normal and vertex position to OpenGL
            glVertex3f(vertex->currentPosition.x(), vertex->currentPosition.y(), vertex->currentPosition.z());
            edge = edge->nextEdge;
            normalIndex++;
        } while (edge != face->edge);
        glEnd();
    }
    glEnable(GL_LIGHTING);
}

/**
 * Draws a face
 *
 * @param face The face to be drawn
 */
void GLContext::drawFace(Face *face) {
    glBegin(GL_POLYGON);

    //get the normal for the face
    int normalIndex = 0;
    QVector3D normal = subdivision.getMesh().normals[face->normalID].normalized();
    //loop through each edge of the face
    HalfEdge *edge = face->edge;
    do {
        //get the vertex for the current edge
        Vertex *vertex = edge->vertex;

        //give the normal and vertex position to OpenGL
        glNormal3f(normal.x(), normal.y(), normal.z());
        glVertex3f(vertex->currentPosition.x(), vertex->currentPosition.y(), vertex->currentPosition.z());
        edge = edge->nextEdge;
        normalIndex++;
    } while (edge != face->edge);
    glEnd();
}

