#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <map>
#include <functional>
#include <chrono>

#include <QMouseEvent>
#include <QVector4D>
#include <QVector3D>
#include <QMatrix4x4>
#include <QVBoxLayout>
#include <QComboBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QSpinBox>
#include <QPushButton>
#include <QFileDialog>
#include <QtCore/qstring.h>
#include <QWidget>
#include <QtOpenGL/QGLWidget>
#include <QtMath>

#include <GL/glu.h>

