// Callum Perks
// MeshSubdivision.h
// Contains a class representing the subdivision of a mesh

#ifndef CATMULL_CLARK_MESHSUBDIVISION_H
#define CATMULL_CLARK_MESHSUBDIVISION_H


#include "Mesh.h"

class MeshSubdivision {
public:
    MeshSubdivision();
    MeshSubdivision(Mesh* mesh);

    void subdivide();
    void subdivideLevels(int level);
    void computeSubdivisionPoints();

    inline Mesh& getMesh(){return *currentMesh;}
    inline Mesh& getOriginalMesh(){return *originalMesh;}
    inline int getSubdivisionLevel() { return currentLevel;}
private:
    Mesh* originalMesh;
    Mesh* currentMesh;
    Mesh* newMesh;

    std::vector<Vertex*> vertices;
    int currentLevel;

    void computeFacePoints();
    void computeEdgePoints();
    void computeVertexPoints();
    void createNewMesh();
    Mesh* originalMeshCleaned();
};


#endif //CATMULL_CLARK_MESHSUBDIVISION_H
