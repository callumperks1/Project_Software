// Callum Perks
// Settings.h
// Contains a simple class that contains the current state of all the settings that the application uses

#ifndef CATMULL_CLARK_SETTINGS_H
#define CATMULL_CLARK_SETTINGS_H

class Settings {
public:
    static bool wireframe;
    static bool facePoints;
    static bool edgePoints;
    static bool vertexPoints;
};

#endif //CATMULL_CLARK_SETTINGS_H
