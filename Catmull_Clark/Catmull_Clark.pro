#-------------------------------------------------
#
# Project created by QtCreator 2018-11-26T15:33:01
#
#-------------------------------------------------

QT       += core gui widgets opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Catmull_Clark
TEMPLATE = app
CONFIG += precompile_header optimize_full

PRECOMPILED_HEADER = pch.h

SOURCES += main.cpp\
           UI/MainWindow.cpp\
           UI/Sidebar.cpp\
           UI/Ui.cpp\
           Interaction\ArcBall.cpp\
           Interaction\MousePicker.cpp\
           Interaction\InputController.cpp\
           GLContext.cpp\
           Mesh.cpp\
           MeshSubdivision.cpp\
           Settings.cpp

HEADERS  += pch.h\
            UI/MainWindow.h\
            UI/Sidebar.h\
            UI/Ui.h\
            Interaction\ArcBall.h\
            Interaction\MousePicker.h\
            Interaction\InputController.h\
            GLContext.h\
            Mesh.h\
            MeshSubdivision.h\
            Settings.h\

LIBS += -lGLU

QMAKE_CXXFLAGS += -std=c++11
QMAKE_LFLAGS += -O3
