// Callum Perks
// MousePicker.cpp
// Contains the class that deals with picking of vertices

#ifndef CATMULL_CLARK_MOUSEPICKER_H
#define CATMULL_CLARK_MOUSEPICKER_H

#include "Mesh.h"
#include "GLContext.h"

class MousePicker {
public:
    MousePicker(GLContext* context);
    static void checkVertices(float mouseX, float mouseY);
    static void moveVertex(float mouseX, float mouseY);

    inline static bool vertexIsPicked(){return vertexPicked;}
    inline static Vertex* currentPickedVertex(){ return currentVertex;}
    static void setVertexIsPicked(bool picked);

private:
    static QMatrix4x4 columnToRowMajor(float matrix[]);

    static Vertex* currentVertex;
    static QMatrix4x4 currentProjMatrix;
    static QMatrix4x4 currentModelMatrix;
    static bool vertexPicked;
    static GLContext* context;
    static float clickRadius;
};


#endif //CATMULL_CLARK_MOUSEPICKER_H
