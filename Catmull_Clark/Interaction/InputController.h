// Callum Perks
// MousePicker.cpp
// Contains the class that handles all input

#ifndef CATMULL_CLARK_INPUTCONTROLLER_H
#define CATMULL_CLARK_INPUTCONTROLLER_H


#include "GLContext.h"
#include "ArcBall.h"
#include "MousePicker.h"

class InputController : public QWidget {
    Q_OBJECT
public:
    InputController(GLContext* context);
    bool eventFilter(QObject *obj, QEvent *event) override;
    GLContext* context;

    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* ) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void wheelEvent(QWheelEvent* event) override;
    void keyPressEvent(QKeyEvent* event) override;

public slots:
    void loadEmbeddedModel(const QString& file);
    void loadModel();
    void setWireframeMode(bool wireframe);
    void renderFacePoints(bool render);
    void renderEdgePoints(bool render);
    void renderVertexPoints(bool render);
    void setSubdivisionLevel(int level);
    void saveModel();
};


#endif //CATMULL_CLARK_INPUTCONTROLLER_H
