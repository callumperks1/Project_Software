// Callum Perks
// MousePicker.cpp
// Contains the implementation of the MousePicker class

#include "pch.h"
#include "MousePicker.h"

GLContext* MousePicker::context = nullptr;
Vertex* MousePicker::currentVertex = nullptr;
bool MousePicker::vertexPicked = false;
QMatrix4x4 MousePicker::currentProjMatrix;
QMatrix4x4 MousePicker::currentModelMatrix;
float MousePicker::clickRadius = 0.02;

MousePicker::MousePicker(GLContext* context){
    this->context = context;
}

/**
 * Loops through all vertices, checking if any of them have been clicked on
 *
 * @param mouseX The mouses X position
 * @param mouseY The mouses Y position
 */
void MousePicker::checkVertices(float mouseX, float mouseY) {
    float currentTransformedZ = 100000000;

    //map the window coordinates to opengl coordinates
    float mappedX = ((2.0f * mouseX) / context->width() - 1.0f);
    float mappedY = -((2.0f * mouseY) / context->height() - 1.0f);

    float projectionMatrixColumn[16];
    float modelMatrixColumn[16];
    glGetFloatv(GL_PROJECTION_MATRIX, projectionMatrixColumn);
    glGetFloatv(GL_MODELVIEW_MATRIX, modelMatrixColumn);

    currentProjMatrix = columnToRowMajor(projectionMatrixColumn);
    currentModelMatrix = columnToRowMajor(modelMatrixColumn);

    //loop through vertices checking if they have been clicked on
    for (Vertex *vertex : context->getSubdivisionMesh().getMesh().vertices) {
        //multiply by all matrices to convert to NDC
        QVector4D transformedVert = currentProjMatrix * currentModelMatrix * vertex->currentPosition;
        transformedVert /= transformedVert.w();

        //Check if the mouse is within a certain range of the vertices for usability
        if ((mappedX < (transformedVert.x() + clickRadius) && mappedX > (transformedVert.x() - clickRadius)) &&
            (mappedY < (transformedVert.y() + clickRadius) && mappedY > (transformedVert.y() - clickRadius))) {
            vertexPicked = true;
            if(transformedVert.z() < currentTransformedZ){
                    currentVertex = vertex;
                    currentTransformedZ = transformedVert.z();
            }
        }
    }
}

/**
 * Moves the vertex that is currently clicked on
 *
 * @param mouseX The mouses X position
 * @param mouseY The mouses Y position
 */
void MousePicker::moveVertex(float mouseX, float mouseY) {
    //Map the mouse coordinates to between -1 and 1
    mouseX = ((2.0f * mouseX) / context->width() - 1.0f);
    mouseY = -((2.0f * mouseY) / context->height() - 1.0f);

    //convert currently clicked vertex to NDC
    QVector4D currentVert = currentProjMatrix * currentModelMatrix * currentVertex->currentPosition;
    currentVert /= currentVert.w();

    //compute the nev vertex position based on the mouse position
    QVector4D vertex = (currentModelMatrix.inverted() * currentProjMatrix.inverted() *
                        (QVector4D(mouseX, mouseY, currentVert.z(), 1)));
    vertex /= vertex.w();

    MousePicker::currentVertex->currentPosition.setX(vertex.x());
    MousePicker::currentVertex->currentPosition.setY(vertex.y());

}

/**
 * Converts a matrix from column major to row major
 *
 * @param matrix The column major matrix that is being converted
 * @return The same matrix in row major form and constructed as a QT matrix
 */
QMatrix4x4 MousePicker::columnToRowMajor(float matrix[]) {
    float newMatrix[16];
    //proj/model matrix from row major to column major
    for (int column = 0; column < 4; column++) {
        for (int row = 0; row < 4; row++) {
            newMatrix[row * 4 + column] = matrix[column * 4 + row];
        }
    }
    return QMatrix4x4(newMatrix);
}

void MousePicker::setVertexIsPicked(bool picked) {
    vertexPicked = picked;
    if(!picked)
        currentVertex = nullptr;
}
