// Callum Perks
// ArcBall.cpp
// Contains the implementation of the ArcBall class

#include "pch.h"
#include "ArcBall.h"

QVector3D ArcBall::startVector;
QVector3D ArcBall::endVector;
float ArcBall::startZRotPos;
float ArcBall::endZRotPos;
QMatrix4x4 ArcBall::rotation;

bool ArcBall::rotating = false;
bool ArcBall::rotatingX = false;
bool ArcBall::rotatingY = false;
bool ArcBall::rotatingZ = false;
float ArcBall::radius = 0;
GLContext* ArcBall::context = nullptr;

ArcBall::ArcBall(GLContext* context) {
    ArcBall::context = context;
    radius = (float)context->getHeight() / 2;
}

/**
 * This method is called when the mouse is clicked
 * It does all the necessary things to allow the mesh to be rotated
 *
 * @param xPos The mouses X position
 * @param yPos The mouses Y position
 */
void ArcBall::startRotation(int xPos, int yPos, Qt::Key key) {
    //map the mouse coordinates to NDC
    float ndcX = (float)xPos / context->width() * 2.0f - 1.0f;
    float ndcY = -((float)yPos / context->height() * 2.0f - 1);

    //set the boolean variables depending on whether or not a key was pressed
    rotating = true;
    rotatingX = (key == Qt::Key_X);
    rotatingY = (key == Qt::Key_Y);
    rotatingZ = (key == Qt::Key_Z);

    if(rotatingZ)
        startZRotation(yPos);
    else
        startVector = mapToBall(QVector3D(rotatingY ? 0.0f : ndcX, rotatingX ? 0.0f : ndcY, 0.0f));
}

/**
 * This method is called when the mouse is moved
 * It does all the necessary things to rotate the mesh
 *
 * @param xPos The mouses X position
 * @param yPos The mouses Y position
 */
void ArcBall::updateRotation(int xPos, int yPos) {
    if(rotatingZ){
        updateZRotation(yPos);
    } else {
        //map the mouse coordinates to NDC
        float ndcX = (float) xPos / context->width() * 2.0f - 1.0f;
        float ndcY = -((float) yPos / context->height() * 2.0f - 1);

        //compute the end vector, ignoring one axis if necessary to lock to one axis
        endVector = mapToBall(QVector3D(rotatingY ? 0.0f : ndcX, rotatingX ? 0.0f : ndcY, 0.0f));

        //compute the angle and the axis from the two vectors
        float angle = QVector3D::dotProduct(startVector.normalized(), endVector.normalized()) * 3;
        QVector3D axis = QVector3D::crossProduct(startVector, endVector);

        //multiply by the old rotation to compute the new rotation
        QMatrix4x4 newRotation;
        newRotation.rotate(angle, axis);
        rotation = newRotation * rotation;

        startVector = endVector;
    }
}

/**
 * This method is called when the mouse is released
 * It does all the necessary things to stop the mesh being rotated
 */
void ArcBall::stopRotation() {
    rotating = false;
    rotatingX = false;
    rotatingY = false;
    rotatingZ = false;
}

void ArcBall::startZRotation(float yPos) {
    startZRotPos = -(yPos / context->height() * 2.0f - 1.0f);
}

void ArcBall::updateZRotation(float yPos) {
    //map to NDC
    endZRotPos = -(yPos / context->height() * 2.0f - 1.0f);

    //calculate the mouse difference and compute an angle based on that
    float difference = startZRotPos - endZRotPos;
    float angle = (float) -90 * difference;

    //multiply by the old rotation to compute the new rotation
    QMatrix4x4 newRotation;
    newRotation.rotate(angle, QVector3D(0, 0, 1));
    rotation = newRotation * rotation;

    startZRotPos = endZRotPos;
}

void ArcBall::setRotation(QMatrix4x4 rotMatrix) {
    rotation = rotMatrix;
}

QVector3D ArcBall::mapToBall(const QVector3D& ndcMousePos) {
    QVector3D ballMousePos;
    ballMousePos.setX(ndcMousePos.x() / radius);
    ballMousePos.setY(ndcMousePos.y() / radius);

    //calculate the length of the vector
    float mouseMagnitude = ballMousePos.length();
    //if outside the radius
    if(mouseMagnitude > 1.0f){
        //set to the closest point in the ball, z value of 0
        ballMousePos *= 1.0f / sqrtf(mouseMagnitude);
        ballMousePos.setZ(0);
    } else {
        //calculate z value
        ballMousePos.setZ(sqrtf(1.0f - mouseMagnitude));
    }

    return ballMousePos;
}
