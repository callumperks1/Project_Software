// Callum Perks
// MousePicker.cpp
// Contains the implementation of the InputController class


#include <Settings.h>
#include "InputController.h"

InputController::InputController(GLContext *context) : context(context) {}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

bool InputController::eventFilter(QObject *obj, QEvent *event) {
    switch (event->type()) {
        case QEvent::KeyPress : {
            keyPressEvent((QKeyEvent *) event);
            return true;
        }

        case QEvent::Wheel : {
            wheelEvent((QWheelEvent *) event);
            return true;
        }

        case QEvent::MouseButtonPress : {
            mousePressEvent((QMouseEvent *) event);
            return true;
        }

        case QEvent::MouseButtonRelease : {
            mouseReleaseEvent((QMouseEvent *) event);
            return true;
        }

        case QEvent::MouseMove : {
            mouseMoveEvent((QMouseEvent *) event);
            return true;
        }
        default: {
            return false;
        }
    }
}

#pragma GCC diagnostic pop


/**
 * Executed when a mouse button is pressed
 *
 * @param event The QMouseEvent instance created by QT
 */
void InputController::mousePressEvent(QMouseEvent *event) {
    ArcBall::startRotation(event->x(), event->y(), Qt::Key_Q);
    MousePicker::checkVertices(event->x(), event->y());
}

/**
 * Executed when a mouse button is released
 *
 * @param event The QMouseEvent created by QT
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

void InputController::mouseReleaseEvent(QMouseEvent *event) {
    MousePicker::setVertexIsPicked(false);
    ArcBall::stopRotation();
    context->updateGL();
}

#pragma GCC diagnostic pop


/**
 * Executed when the mouse is moved
 *
 * @param event The QMouseEvent created by QT
 */
void InputController::mouseMoveEvent(QMouseEvent *event) {
    if (MousePicker::vertexIsPicked()) {
        MousePicker::moveVertex(event->x(), event->y());
        context->getMesh().recalculateNormals();
        context->getSubdivisionMesh().computeSubdivisionPoints();
    } else if (ArcBall::isRotating()) {
        ArcBall::updateRotation(event->x(), event->y());
    }
    context->updateGL();
}

/**
 * Executed when the mouse wheel is moved
 *
 * @param event The QWheelEvent created by QT
 */
void InputController::wheelEvent(QWheelEvent *event) {
    float delta = (event->angleDelta() / 8).y();
    context->setZoom(context->getZoom() + delta / 100);

    context->updateGL();
}

void InputController::keyPressEvent(QKeyEvent *event) {
    switch (event->key()) {
        case Qt::Key_R :
            ArcBall::setRotation(QMatrix4x4());
            break;
        case Qt::Key_X:
            if (ArcBall::isRotatingX())
                ArcBall::stopRotation();
            else
                ArcBall::startRotation(QCursor::pos().x(), QCursor::pos().y(), Qt::Key_X);
            break;

        case Qt::Key_Y:
            if (ArcBall::isRotatingY())
                ArcBall::stopRotation();
            else
                ArcBall::startRotation(QCursor::pos().x(), QCursor::pos().y(), Qt::Key_Y);
            break;
        case Qt::Key_Z:
            if (ArcBall::isRotatingZ())
                ArcBall::stopRotation();
            else
                ArcBall::startRotation(QCursor::pos().x(), QCursor::pos().y(), Qt::Key_Z);
            break;
        case Qt::Key_V:
            context->getSubdivisionMesh().getMesh().resetVertices();
            break;
    }

    context->updateGL();
}

void InputController::loadEmbeddedModel(const QString &file) {
    context->setMesh(Mesh::creatFromObj("../Catmull_Clark/res/" + file.toStdString() + ".obj"));
    context->setSubdivisionMesh(MeshSubdivision(&context->getMesh()));
    context->updateGL();
}

void InputController::loadModel() {
    std::string str = QFileDialog::getOpenFileName().toStdString();
    if (str.empty())
        return;

    context->setMesh(Mesh::creatFromObj(str));
    context->setSubdivisionMesh(MeshSubdivision(&context->getMesh()));
    context->updateGL();
}

void InputController::setWireframeMode(bool wireframe) {
    Settings::wireframe = wireframe;
    glPolygonMode(GL_FRONT_AND_BACK, Settings::wireframe ? GL_LINE : GL_FILL);
    context->updateGL();
}

void InputController::renderFacePoints(bool render) {
    Settings::facePoints = render;
    context->updateGL();
}

void InputController::renderEdgePoints(bool render) {
    Settings::edgePoints = render;
    context->updateGL();
}

void InputController::renderVertexPoints(bool render) {
    Settings::vertexPoints = render;
    context->updateGL();
}

void InputController::setSubdivisionLevel(int level) {
    context->getSubdivisionMesh().subdivideLevels(level);
    context->updateGL();
}

void InputController::saveModel() {
    MeshSubdivision &subdivision = context->getSubdivisionMesh();
    std::ofstream outputFile(QFileDialog::getSaveFileName().toStdString());
    //output each vertex to the file
    for (Vertex *vertex : subdivision.getMesh().vertices)
        outputFile << "v " << vertex->currentPosition.x() << " " << vertex->currentPosition.y() << " "
                   << vertex->currentPosition.z() << std::endl;

    for (const QVector3D &vertex : subdivision.getMesh().normals)
        outputFile << "vn " << vertex.x() << " " << vertex.y() << " " << vertex.z() << std::endl;


    //loop through each face
    for (Face *face : subdivision.getMesh().faces) {
        //output the preceding 'f'
        outputFile << "f ";
        //loop through the vertices
        HalfEdge *edge = face->edge;
        do {
            //find vertex index
            for (unsigned int i = 0; i < subdivision.getMesh().vertices.size(); i++) {
                if (subdivision.getMesh().vertices[i] == edge->vertex) {
                    //output the index to the file, offset by 1 so indexing starts at 1 not 0
                    outputFile << i + 1 << "//" << face->normalID + 1 << " ";
                    break;
                }
            }

            edge = edge->nextEdge;
        } while (edge != face->edge);
        outputFile << std::endl;
    }
    outputFile.close();
}
