// Callum Perks
// ArcBall.cpp
// Contains the class the rotates the mesh

#ifndef CATMULL_CLARK_ARCBALL_H
#define CATMULL_CLARK_ARCBALL_H

#include "pch.h"
#include "GLContext.h"

class ArcBall {
public:
    ArcBall(GLContext* context);

    static void startRotation(int xPos, int yPos, Qt::Key key);
    static void updateRotation(int xPos, int yPos);
    static void stopRotation();
    static void setRotation(QMatrix4x4 rotMatrix);

    inline static bool isRotating(){return rotating;}
    inline static bool isRotatingX(){return rotatingX;}
    inline static bool isRotatingY(){return rotatingY;}
    inline static bool isRotatingZ(){return rotatingZ;}

    static QMatrix4x4 rotation;
private:
    static void startZRotation(float yPos);
    static void updateZRotation(float yPos);
    static QVector3D mapToBall(const QVector3D& ndcMousePos);

    static QVector3D startVector, endVector;
    static float startZRotPos, endZRotPos;
    static float radius;

    static GLContext* context;

    static bool rotating;
    static bool rotatingX;
    static bool rotatingY;
    static bool rotatingZ;
};


#endif //CATMULL_CLARK_ARCBALL_H
