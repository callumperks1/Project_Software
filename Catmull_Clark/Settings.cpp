// Callum Perks
// Settings.cpp
// Contains a simple class that contains the definition of the varibales in the Settings class

#include "Settings.h"

bool Settings::wireframe = false;
bool Settings::facePoints = false;
bool Settings::edgePoints = false;
bool Settings::vertexPoints = false;
