// Callum Perks
// GLContext.h
// Contains a simple wrapper class for QT QGLWidget

#ifndef CATMULL_CLARK_GLCONTEXT_H
#define CATMULL_CLARK_GLCONTEXT_H

#include "pch.h"
#include "Mesh.h"
#include "MeshSubdivision.h"

class GLContext : public QGLWidget {
    Q_OBJECT
public:
    GLContext();
     ~GLContext();

    inline Mesh& getMesh(){return mesh;}
    inline MeshSubdivision& getSubdivisionMesh(){return subdivision;}
    inline int getWidth(){return widgetWidth;};
    inline int getHeight(){ return widgetHeight;};
    inline float getZoom(){ return zoom;};

    inline void setMesh(Mesh mesh){this->mesh = mesh;}
    inline void setSubdivisionMesh(MeshSubdivision mesh){this->subdivision = mesh;}
    inline void setZoom(float zoom){this->zoom = zoom;}

    void initializeGL() override;
    void paintGL() override;

private:
    void drawMesh(const Mesh& mesh);
    void drawOriginalMesh(const Mesh& mesh);
    void drawFace(Face* face);

    int widgetWidth, widgetHeight;
    float zoom;

    Mesh mesh;
    MeshSubdivision subdivision;
};



#endif //CATMULL_CLARK_GLCONTEXT_H
