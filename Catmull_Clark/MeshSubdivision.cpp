// Callum Perks
// MeshSubdivision.h
// Contains the implementation of the MeshSubdivision class

#include "MeshSubdivision.h"

MeshSubdivision::MeshSubdivision() {}

MeshSubdivision::MeshSubdivision(Mesh *mesh) : originalMesh(mesh), currentMesh(mesh), currentLevel(0) {
    newMesh = new Mesh();
    computeSubdivisionPoints();
}

/**
 * Subdivides the mesh by the specified number of widgets
 *
 * @param level The number of levels to subdivide by
 */
void MeshSubdivision::subdivideLevels(int level) {
    // If the level is greater than the current level
    if (level > currentLevel) {
        for (int i = 0; i < level - currentLevel; i++) {
            subdivide();
        }
    } else {
        //otherwise clear everything and subdivide from the original mesh
        vertices.clear();
        currentMesh = originalMeshCleaned();
        computeSubdivisionPoints();
        for (int i = 0; i < level; i++) {
            subdivide();
        }
    }
    currentLevel = level;
}

/**
 * Subdivide the mesh once
 */
void MeshSubdivision::subdivide() {
    createNewMesh();

    currentMesh = newMesh;
    newMesh = new Mesh();
    computeSubdivisionPoints();
}

/**
 * Computes the new face points for the current mesh
 */
void MeshSubdivision::computeFacePoints() {
    for (Face *face : currentMesh->faces) {
        QVector4D sum;
        int count = 0;

        //loop through all vertices for that face by traversing the edges of the face
        HalfEdge *edge = face->edge;
        do {
            //add all the vertex positions together
            Vertex *vertex = edge->vertex;
            sum += vertex->currentPosition;
            count++;
            edge = edge->nextEdge;
        } while (edge != face->edge);
        //divide to find the average
        sum /= count;

        Vertex *newVertex = new Vertex{sum, sum, nullptr, nullptr};
        face->facePoint = newVertex;
        vertices.push_back(newVertex);
    }
}

/**
 * Computes the new edge points for the current mesh
 */
void MeshSubdivision::computeEdgePoints() {
    for (HalfEdge *edge : currentMesh->edges)
        edge->edgePoint = nullptr;

    for (Face *face : currentMesh->faces) {
        //loop through each edge
        HalfEdge *edge = face->edge;
        do {
            //sum the face points and vertices for that edge
            QVector4D sum;
            sum += edge->vertex->currentPosition + edge->nextEdge->vertex->currentPosition;

            QVector4D vertex1 = edge->face->facePoint->currentPosition;
            QVector4D vertex2 = edge->twinEdge->face->facePoint->currentPosition;
            sum += (vertex1 + vertex2);
            sum /= 4;

            Vertex *newVertex = new Vertex{sum, sum, nullptr, nullptr};

            //only assign if this edge point hasn't already been calculated
            if (!edge->edgePoint) {
                edge->edgePoint = newVertex;
                edge->twinEdge->edgePoint = newVertex;
                vertices.push_back(newVertex);
            }
            edge = edge->nextEdge;
        } while (edge != face->edge);
    }
}

/**
 * Computes the new vertex points for the current mesh
 */
void MeshSubdivision::computeVertexPoints() {
    //loop through each vertex
    for (Vertex *vertex : currentMesh->vertices) {
        int degree = currentMesh->calculateVertexDegree(vertex);
        QVector4D facePointAverage;
        QVector4D midpointAverage;

        //check if any vertices are sharp
        HalfEdge *edge = vertex->outEdge;
        int count = 0;
        do {
            count++;
            //calculate face point average
            facePointAverage += edge->face->facePoint->currentPosition;
            //calculate the edge midpoint average
            midpointAverage += (edge->vertex->currentPosition + edge->nextEdge->vertex->currentPosition) / 2;

            edge = edge->twinEdge->nextEdge;
        } while (edge != vertex->outEdge);
        facePointAverage /= count;
        midpointAverage /= count;

        //calculate the overall average
        QVector4D average = (facePointAverage / degree) + (midpointAverage / degree * 2) +
                            (vertex->currentPosition / degree * (degree - 3));

        Vertex *newVertex = new Vertex{average, average, nullptr, nullptr};
        vertex->newVertex = newVertex;
        vertices.push_back(newVertex);
    }
}


/**
 * Constructs a new mesh from the old mesh and the newly generated points
 */
void MeshSubdivision::createNewMesh() {
    newMesh->vertices = vertices;
    vertices.clear();

    //normal id used for keeping tack of which normal the face should use
    int normalID = 0;

    for (Face *face : currentMesh->faces) {

        //loop through the faces edgesMap
        //create one new face for each old edge
        HalfEdge *edge = face->edge;
        do {
            std::vector<Vertex *> faceVertices;
            //New vertices
            Vertex *vertex1 = face->facePoint;
            Vertex *vertex2 = edge->edgePoint;
            Vertex *vertex3 = edge->nextEdge->vertex->newVertex;
            Vertex *vertex4 = edge->nextEdge->edgePoint;

            faceVertices.push_back(vertex1);
            faceVertices.push_back(vertex2);
            faceVertices.push_back(vertex3);
            faceVertices.push_back(vertex4);

            newMesh->createFace(faceVertices, normalID++);

            edge = edge->nextEdge;
        } while (edge != face->edge);
    }
}

/**
 * Deletes the edge points from the original mesh
 *
 * @return The original mesh
 */
Mesh *MeshSubdivision::originalMeshCleaned() {
    for (HalfEdge *edge : originalMesh->edges)
        edge->edgePoint = nullptr;

    return originalMesh;
}

void MeshSubdivision::computeSubdivisionPoints() {
    vertices.clear();
    computeFacePoints();
    computeEdgePoints();
    computeVertexPoints();

}
