// Callum Perks
// Mesh.h
// Contains all structs/classes needed to construct
// and represent a half edge

#ifndef CATMULL_CLARK_MESH_H
#define CATMULL_CLARK_MESH_H

#include "pch.h"

struct HalfEdge;

struct Vertex{
    QVector4D originalPosition;
    QVector4D currentPosition;
    HalfEdge* outEdge;
    Vertex* newVertex;
};

struct Face{
    HalfEdge* edge;
    Vertex* facePoint;
    std::vector<int> vertexID; //used only for drawing the mesh
    int normalID; //used only for drawing the mesh

};

struct HalfEdge{
    Vertex* vertex;
    Face* face;
    HalfEdge* nextEdge;
    Vertex* edgePoint;
    HalfEdge *twinEdge;
};

/*
 * Mesh class representing a half edge mesh
 */
class Mesh {
public:
    Mesh();
    Mesh(std::vector<Vertex *> &vertices, std::map<std::pair<int, int>, HalfEdge *> &edges,
             std::vector<Face *> &faces, std::vector<QVector3D> normals);
    ~Mesh();
    static Mesh creatFromObj(const std::string& filename);

    void createFace(const std::vector<Vertex *>& faceVertices, int normalID);
    void recalculateNormals();
    int calculateVertexDegree(Vertex* vertex);

    void resetVertices();

    std::vector<Vertex*> vertices;
    std::map<std::pair<int, int>, HalfEdge*> edgesMap; //used if created from an obj file
    std::vector<HalfEdge*> edges; // used when created from subdivision
    std::vector<Face*> faces;
    std::vector<QVector3D> normals;
};

void parseLine(const std::string &line, std::vector<Vertex *> &vertices, std::vector<Face *> &faces,
               std::vector<QVector3D>& normals);
std::map<std::pair<int, int>, HalfEdge*> createHalfEdges(std::vector<Vertex*>& vertices, std::vector<Face*>& faces);

#endif //CATMULL_CLARK_MESH_H
