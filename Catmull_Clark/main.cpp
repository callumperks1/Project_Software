#include "UI/MainWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{

    QApplication app(argc, argv);
    MainWindow* w = new MainWindow();
    w->resize(1280, 720);
    w->show();

    app.exec();

    delete w;
    return 0;
}
