// Callum Perks
// ManWindow.cpp
// Contins the implementation of the MainWindow class

#include "MainWindow.h"

MainWindow::MainWindow(){
    interface = new Ui();
    this->setLayout(interface);
}

MainWindow::~MainWindow(){
    delete interface;
}
