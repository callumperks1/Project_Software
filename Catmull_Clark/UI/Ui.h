// Callum Perks
// Ui.h
// Contains the main class representing the full UI of the application

#ifndef UI_H
#define UI_H

#include "pch.h"
#include <UI/Sidebar.h>
#include <GLContext.h>
#include <Interaction/InputController.h>

class Ui : public QHBoxLayout{
public:
    Ui();
    ~Ui();

private:
    Sidebar* sidebar = nullptr;
    GLContext* context = nullptr;
    InputController* controller = nullptr;
};

#endif

