// Callum Perks
// Ui.cpp
// Contains the implementation of the Ui class

#include <Interaction/ArcBall.h>
#include <Interaction/MousePicker.h>
#include "Ui.h"

Ui::Ui(){
    sidebar = new Sidebar();
    context = new GLContext();
    controller = new InputController(context);
    context->installEventFilter(controller);
    MousePicker mousePicker(context);
    this->addLayout(sidebar);
    this->addWidget(context);
    this->setStretch(0, 1);
    this->setStretch(1, 7);
    ArcBall arcBall(context);


    connect(sidebar->getModelFile(), SIGNAL(activated(const QString&)), controller, SLOT(loadEmbeddedModel(const QString&)));
    connect(sidebar->getWireframe(), SIGNAL(toggled(bool)), controller, SLOT(setWireframeMode(bool)));
    connect(sidebar->getFacePoints(), SIGNAL(toggled(bool)), controller, SLOT(renderFacePoints(bool)));
    connect(sidebar->getEdgePoints(), SIGNAL(toggled(bool)), controller, SLOT(renderEdgePoints(bool)));
    connect(sidebar->getVertexPoints(), SIGNAL(toggled(bool)), controller, SLOT(renderVertexPoints(bool)));
    connect(sidebar->getSubdivisionLevel(), SIGNAL(valueChanged(int)), controller, SLOT(setSubdivisionLevel(int)));
    connect(sidebar->getOpenButton(), SIGNAL(pressed()), controller, SLOT(loadModel()));
    connect(sidebar->getSaveButton(), SIGNAL(pressed()), controller, SLOT(saveModel()));
}

Ui::~Ui(){
    delete sidebar;
    delete context;
    delete controller;
}
