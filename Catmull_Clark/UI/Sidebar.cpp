// Callum Perks
// Sidebar.cpp
// Contains the implementation of the Sidebar class

#include "Sidebar.h"
#include <iostream>
#include <dirent.h>

Sidebar::Sidebar() {
    divisionLevel = new QSpinBox();
    modelFile = new QComboBox();
    wireframe = new QCheckBox(QString("Wireframe"));
    facePoints = new QCheckBox(QString("Show face points"));
    edgePoints = new QCheckBox(QString("Show edge points"));
    vertexPoints = new QCheckBox(QString("Show vertex points"));
    openButton = new QPushButton("Open");
    saveButton = new QPushButton("Save");

    //read in the available models
    DIR *dp;
    struct dirent *dirp;
    if ((dp = opendir("../Catmull_Clark/res")) == NULL) {
        std::cout << "Error(" << errno << ") opening " << "res" << std::endl;
    }

    modelFile->addItem(QString("cube"));

    while ((dirp = readdir(dp)) != NULL) {
        if(strcmp(dirp->d_name, ".") != 0 && strcmp(dirp->d_name, "..") != 0 &&  strcmp(dirp->d_name, "cube.obj") != 0) {
            std::string name = dirp->d_name;
            name = name.substr(0, name.length() - 4);
            modelFile->addItem(QString(name.c_str()));
        }
    }
    closedir(dp);

    this->addWidget(divisionLevel);
    this->addWidget(modelFile);
    this->addWidget(wireframe);
    this->addWidget(facePoints);
    this->addWidget(edgePoints);
    this->addWidget(vertexPoints);
    this->addWidget(openButton);
    this->addWidget(saveButton);

}

Sidebar::~Sidebar() {
    delete divisionLevel;
    delete modelFile;
    delete wireframe;
    delete facePoints;
    delete edgePoints;
    delete vertexPoints;
    delete openButton;
    delete saveButton;
}
