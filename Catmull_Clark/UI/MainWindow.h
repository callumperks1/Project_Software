// Callum Perks
// MainWindow.h
// Contains a simple class representing the window of the application

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#if 0
#include <QWidget>
#endif

#include "pch.h"

#include "Sidebar.h"
#include "Ui.h"

class MainWindow : public QWidget{
public:
    MainWindow();
    ~MainWindow();

private:
    Ui *interface = nullptr;
};

#endif
