// Callum Perks
// GLContext.h
// Contains a class representing the sidebar of the application

#ifndef SIDEBAR_H
#define SIDEBAR_H

#include "pch.h"

class Sidebar : public QVBoxLayout{
public:
    Sidebar();
    ~Sidebar();

    inline QComboBox* getModelFile(){ return modelFile;}
    inline QCheckBox* getWireframe(){ return wireframe;}
    inline QCheckBox* getFacePoints(){ return facePoints;}
    inline QCheckBox* getEdgePoints(){ return edgePoints;}
    inline QCheckBox* getVertexPoints(){ return vertexPoints;}
    inline QSpinBox* getSubdivisionLevel(){ return divisionLevel;}
    inline QPushButton* getOpenButton(){ return openButton;}
    inline QPushButton* getSaveButton(){ return saveButton;}
private:
    QSpinBox* divisionLevel = nullptr;
    QComboBox* modelFile = nullptr;
    QCheckBox* wireframe = nullptr;
    QCheckBox* facePoints = nullptr;
    QCheckBox* edgePoints = nullptr;
    QCheckBox* vertexPoints = nullptr;
    QPushButton* openButton = nullptr;
    QPushButton* saveButton = nullptr;
};

#endif

