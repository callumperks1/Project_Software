# Catmull Clark Subdivision Surfaces
This is a simple implementation of the Catmull Clark method of surface subdivision written in C++ with the help of the QT and OpenGL libraries.

## Meshes
There are a number of built in meshes that can be used. These meshes can be chosen from the drop down box in the settings pane. If you would like to use your own mesh, you can do so with the open button. The mesh file must be int he OBJ format. Once edited, the mesh can be saved to a new OBJ file with the save button.

## Interaction 
The mesh can be rotated by simply clicking and dragging the mouse around the window. The vertices of the mesh can be moved by clicking on them and then moving the mouse. Both of these changes can be reset by pressing the 'r' and 'v' keys respectively. If you wish to rotate the object around one axis only, this can be done by first pressing one of the 'x', 'y', or 'z' keys ad then dragging the mouse without clicking first.

## Subdivision
The subdivision level can be changed to any desired level with the given spinbox. High levels will take a while to run. The points used to ceate the new mesh can be shown with the checkboxes in the settings pane. It is recommended to also have the mesh rendered in wireframe mode when viewing these points as they usually move inwards and so can not be seen otherwise.

